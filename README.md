# cpu-govset

A simple Linux script/CLI app to change CPU frequency governors in order to balance performance vs battery life depending on your intended workload.

Currently only tested with Intel CPUs.

Released under GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html

Dependencies: cpupower

# What this app does:

1. Checks if the dependencies required (cpupower) are installed.
2. If they are not installed, it will try to do so using commonly found package managers (apt, pacman, dnf or zypper).
3. If it is unable to install the required dependencies automatically, you will need to do so manually via your distribution's installation method.
4. Shows the current CPU frequency governor in use
5. Displays available CPU frequency governors available
6. Activates the selected CPU frequency governor

**NB. On reboot, the default CPU governor will be reset. If you need to change the default behaviour on boot, please edit:**

	/etc/default/cpupower
	
Then start the `cpupower.service` using `systemd`:

	sudo systemctl enable --now cpupower.service

# How to use (if you don't have git):

1. Download or copy the script to a text file named "cpu-govset"
2. Make the file executable by:

	- right clicking the file in your file manager, choosing properties and marking it as executable; OR
	- in terminal:

			chmod +x cpu-govset

3. Execute the script by opening a terminal in the same folder and running:

		./cpu-govset

# How to use (with git):

1. git clone this repository

		git clone https://gitlab.com/sleepyeyesvince/cpu-govset.git

2. Make the file executable by:

	- right clicking the file in your file manager, choosing properties and marking it as executable; OR
	- in terminal:

			cd cpu-govset
			chmod +x cpu-govset

3. Execute the script by opening a terminal in the same folder and running:

		./cpu-govset

[Optional: You could place the script in a folder contained in $PATH such has ~/bin or ~/.bin to be able to run the script in terminal from anywhere in your system]
